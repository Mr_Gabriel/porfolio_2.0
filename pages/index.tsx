import type { NextPage } from 'next'
import Head from 'next/head'
import Header from "../components/Header"


const Home: NextPage = () => {
  return (
    <div>
      <Head>
        <title>Gabriel New Porfolio</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

        {/* Header */}
        <Header />
        {/* Hero */}

        {/* About */}

        {/* Experience */}

        {/* Skills */}

        {/* Projects */}

        {/* Contact me */}

    </div>
  )
}

export default Home
